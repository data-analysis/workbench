var path = require('path');

module.exports = {
  dist: path.resolve(path.join(__dirname, 'dist'))
}
