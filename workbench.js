// This is an entry file - consider it special - load what you need, but do your work elsewhere (ex. web_modules) //
// require("babel-polyfill");

var info = require("./package.json");
require("jquery-ui/widget");
require('h13a-os-shellutils')({namespace: info.name});
require("./styles.scss");
require('h13a-container')({info: info});
