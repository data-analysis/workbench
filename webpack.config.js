var webpack = require('webpack');

var ExtractTextPlugin = require("extract-text-webpack-plugin");

var devFlagPlugin = new webpack.DefinePlugin({
  DEVELOPER: JSON.stringify(JSON.parse(process.env.DEBUG || 'false')),
  BUILD: JSON.stringify(JSON.parse(process.env.BUILD || 'false'))
});

new webpack.ProvidePlugin({

  $: "jquery",
  jQuery: "jquery",
  "window.jQuery": "jquery",
  "window.$": "jquery"
})

var path = require('path');

module.exports = {
  plugins: [new ExtractTextPlugin("[name].css"), devFlagPlugin],
  resolve: {
    root: [
      path.resolve('./dev_modules/'), // you can put your modules into dev_modules
    ]
  },

  entry: {
    // extra plugins, may not always be required
    "plugin.concepts": "./plugin.concepts.js",
    "plugin.charts": "./plugin.charts.js",

    // the main web program //
    "workbench": "./workbench.js",

   },

  output: {

    path: __dirname + '/dist',
    filename: "[name].min.js",
    chunkFilename: "program.chunk-[id].js"

  },

  module: {

    loaders: [

        {
          test: /\.(jpe?g|png|gif|svg)$/i,
          loaders: [
            'file?hash=sha512&digest=hex&name=[hash].[ext]',
            'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
          ]
        },

        {
          test: /\.html$/,
          loader: 'raw-loader',
          exclude: /h13a-wwwroot-iota/,
        },

        {
          test: /\.md$/,
          loader: "html!markdown"
        },

        {
          test: /theme.theme\d+.color.scss$/,
          loader: ExtractTextPlugin.extract("style-loader", "css!sass"),
        },

        {
          test: /theme.theme\d+.contrast.scss$/,
          loader: ExtractTextPlugin.extract("style-loader", "css!sass"),
        },

        {
          test: /loaders.css.src.loaders.scss$/,
          loader: ExtractTextPlugin.extract("style-loader", "css!sass"),
        },

        {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract("style-loader", "css!sass"),
          exclude: /theme..contrast.scss|theme..color.scss|loaders.css.src.loaders.scss/,
        }, {
          test: /\.css$/,
          loader: ExtractTextPlugin.extract("style-loader", "css")
        },

        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "url-loader?limit=10000&mimetype=application/font-woff"
        },

        {
          test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "file-loader"
        },

        {
          test: /\.json$/,
          loader: 'json-loader'
        },

        {
          test: /node.modules\/h13a\-.+\.es6$/,
          loader: 'babel',
          query: {
            presets: ['react', 'es2015', "stage-0"],
            plugins: ["syntax-async-functions", "transform-async-to-generator"],
          }
        },

        {
          test: /\.jsx?$|\.es6$/,
          exclude: /(node_modules|bower_components)/,
          loader: 'babel',
          query: {
            presets: ['react', 'es2015', "stage-0"],
            plugins: ["syntax-async-functions", "transform-async-to-generator"],
          }
        }

      ] // loaders

  } // module

}; // exports
